// pragma solidity ^0.5.0;
pragma solidity >=0.4.20;
pragma experimental ABIEncoderV2;
// pragma solidity ^0.8.7;


contract Pristine {

    mapping(string => Product) products; // This is a map of { epcCode: Product }
    function getProduct(string memory epcCode) view public returns(Product memory) {
        return products[epcCode];
    }
    

    mapping(string => Shipment[]) shipments; // This is a map of { epcCode: Shipment[] }
    function getShipments(string memory epcCode) view public returns(Shipment[] memory) {
        return shipments[epcCode];
    }
    

    uint public participantCount = 0;
    mapping(uint => Participant) participants; // This is a map of { participantId: Participant }
    function getParticipant(uint participantId) view public returns(Participant memory) {
        return participants[participantId];
    }
    

    enum ParticipantType {
        MAN, LOG, DC, RET
    }
    
    
    struct Participant {
        uint id;
        string name;
        ParticipantType participantType;
        string location;
    }


    struct Shipment {
        Participant previousHolder;
        Participant currentHolder;
    }

    event ShipmentCreated(
        Participant previousHolder,
        Participant currentHolder
    );


    struct Product {
        string epcCode;
        string serialNumber;
        string tagId;
        string brand;
        
        Participant currentHolder;
    }

    event ProductCreated(
        string epcCode,
        string serialNumber,
        string tagId,
        string brand,
        Participant currentHolder
    );


    function createProduct(
            string memory _epcCode, 
            string memory _serialNumber,
            string memory _tagId,
            string memory _brand,
            
            uint _currentHolderParticipantId
    ) public {
        
        // Get the current holder info
        Participant memory _currentHolder = getParticipant(_currentHolderParticipantId);
        
        // Create the product in the BC
        products[_epcCode] = Product({
            epcCode:_epcCode, 
            serialNumber: _serialNumber, 
            tagId: _tagId, 
            brand: _brand,
            currentHolder: _currentHolder
        });

        emit ProductCreated(_epcCode, _serialNumber, _tagId, _brand, _currentHolder);
    }


    function createShipment(string memory epcCode, uint currentHolderParticipantId) public {
        // Get the previous holder from the product
        Participant memory previousHolder = getProduct(epcCode).currentHolder;
        
        // Get current holder
        Participant memory currentHolder = getParticipant(currentHolderParticipantId);
        
        // Add a new shipment to the BC
        shipments[epcCode].push(Shipment(previousHolder, currentHolder));
        
        // Update the new holder of the product
        products[epcCode].currentHolder = currentHolder;

        emit ShipmentCreated(previousHolder, currentHolder);
        
    }
    
    
    function createParticipant(string memory name, ParticipantType  participantType, string memory location) public {
        participantCount ++;
        
        participants[participantCount] = Participant(participantCount, name, participantType, location);
    }
    
    
    function loadParticipants() internal {
        // Create 4 participants
        createParticipant("Levi Strauss & Co.", ParticipantType.MAN, "Banglore, India"); // paticipantId = 1
        createParticipant("UPS Supply Chain Solutions", ParticipantType.LOG, "Dubai, UAE"); // // paticipantId = 2
        createParticipant("NRI DC", ParticipantType.DC, "Chicago, IL, USA"); // paticipantId = 3
        createParticipant("Levi's Store Georgetown", ParticipantType.RET, "Washinton, USA"); // paticipantId = 4
    }
    

    function loadProducts() internal {
        // Create 2 products
        createProduct("epc1", "sno1", "tag1", "Levi's", 1);
        createProduct("epc2", "sno2", "tag2", "Levi's", 1);
    }
    
    event dataLoaded();
    function loadData() public {
        loadParticipants();
        loadProducts();
        emit dataLoaded();
    }

}
