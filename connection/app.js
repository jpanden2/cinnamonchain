const { NotFound } = require("http-errors");
const Web3  = require("web3");
const CONTRACT_JSON = require('../build/contracts/Pristine.json');
let CONTRACT_ADDRESS;
let CONTRACT;
let web3;
let ADDRESSES = [];
let FIRST_ADDRESS;

const participantTypes = {
  0: 'MAN',
  1: 'LOG',
  2: 'DC',
  3: 'RET'
}

async function setupContract() {
  if(!CONTRACT) {
    console.log(`****** creating CONTRACT in setup() ... CONTRACT_ADDRESS = ${CONTRACT_ADDRESS}`);
    web3 = new Web3('http://localhost:8545');
    console.log(`web3 = ${web3}`);

    ADDRESSES = await web3.eth.getAccounts();
    console.log(`ADDRESSES = ${ADDRESSES}`);
    FIRST_ADDRESS = ADDRESSES[0];

    const id = await web3.eth.net.getId();
    console.log(`id = ${id}`);

    CONTRACT_ADDRESS = CONTRACT_JSON.networks[id].address;
    console.log(`CONTRACT_ADDRESS = ${CONTRACT_ADDRESS}`);

    CONTRACT = new web3.eth.Contract(CONTRACT_JSON.abi, CONTRACT_ADDRESS);
    console.log(`CONTRACT = ${CONTRACT}`);
  }
}

setupContract();


module.exports = {
  loadData: async function (callback) {
    try {
      console.log(`In loadData() ... CONTRACT_ADDRESS = ${CONTRACT_ADDRESS}`);
      // const balance = await web3.eth.getBalance(FIRST_ADDRESS);
      // console.log(`Balance = ${balance}`);

      console.log("Making call to loadData() function call to the contract ...");
      await CONTRACT.methods.loadData()
      .send({
        from: FIRST_ADDRESS,
        gas: 1000000
      });
      // console.log(result);
      callback('Data loaded successfully!', null);
    } catch(err) {
      console.error(err);
      callback(null, err);
    }
  },

  getProduct: async function (epcCode, callback) {
    try {
      console.log(`In getProduct() ... CONTRACT_ADDRESS = ${CONTRACT_ADDRESS}`);
      console.log("Making call to getProduct() function call to the contract ...");
      const productResult = await CONTRACT.methods.getProduct(epcCode).call();
      console.log("getProduct() function call response ...");
      console.log(productResult);

      // Not found scenario
      if(!productResult.epcCode) {
        callback(null, {error: new NotFound(`ecpCode: ${epcCode} not found!`), status: 404});
        return;
      }

      const response = {
        epcCode: productResult.epcCode,
        serialNumber: productResult.serialNumber,
        tagId: productResult.tagId,
        brand: productResult.brand,
        currentHolder: {
          id: productResult.currentHolder.id,
          name: productResult.currentHolder.name,
          participantType: participantTypes[productResult.currentHolder.participantType],
          location: productResult.currentHolder.location
        },
        shipments: []
      }

      console.log("Making call to getShipments() function call to the contract ...");
      const shipmentResult = await CONTRACT.methods.getShipments(epcCode).call();
      console.log("getShipments() function call response ...");
      console.log(shipmentResult);
      
      for(let i = 0 ; i < shipmentResult.length ; i++) {
        console.log('....', i);
        const result = shipmentResult[i];
        console.log(result);
        response.shipments.push(
          { 
            previousHolder: {
              id: result.previousHolder.id,
              name: result.previousHolder.name,
              participantType: participantTypes[result.previousHolder.participantType],
              location: result.previousHolder.location,
            }, 
            currentHolder: {
              id: result.currentHolder.id,
              name: result.currentHolder.name,
              participantType: participantTypes[result.currentHolder.participantType],
              location: result.currentHolder.location,
            } 
          }
        );
      }

      callback(response, null);
    } catch(err) {
      console.error(err);
      callback(null, err);
    }
  },

  createProduct: async function (body, callback) {
    try {
      console.log(`In createProduct() ... CONTRACT_ADDRESS = ${CONTRACT_ADDRESS}`);
      const { epcCode, serialNumber, tagId, brand, currentHolderParticipantId  } = body;
      console.log("payload below...")
      console.log({epcCode, serialNumber, tagId, brand, currentHolderParticipantId});
      
      // Make the createProduct() contract call
      console.log("Making call to createProduct() function call to the contract ...");
      // If the current holder participant id is not given then add the current user as MANUFACTURER
      let playerId = currentHolderParticipantId;
      if(!currentHolderParticipantId) {
        playerId = 1;
      }
      await CONTRACT.methods.createProduct(epcCode, serialNumber, tagId, brand, playerId)
      .send({
        from: FIRST_ADDRESS,
        gas: 1000000
      });

      // Get the newly created product and return
      const createdProduct = await this.getProduct(epcCode, callback);
      console.log(createdProduct);
      callback(createdProduct, null);

    } catch(err) {
      console.error(err);
      callback(null, err);
    }
  },

  createShipment: async function (body, callback) {
    try {
      console.log(`In createShipment() ... CONTRACT_ADDRESS = ${CONTRACT_ADDRESS}`);
      const { epcCode, currentHolderParticipantId  } = body;
      console.log("payload below...")
      console.log({epcCode, currentHolderParticipantId});

      console.log("Making call to createShipment() function call to the contract ...");
      await CONTRACT.methods.createShipment(epcCode, currentHolderParticipantId)
      .send({
        from: FIRST_ADDRESS,
        gas: 1000000
      });

      // Get the product and return
      const product = await this.getProduct(epcCode, callback);
      console.log(product);
      callback(product, null);     

    } catch(err) {
      console.error(err);
      callback(null, err);
    }
  },

  verifyProduct: async function(epcCode, callback) {
    console.log(`In verifyProduct() ... CONTRACT_ADDRESS = ${CONTRACT_ADDRESS}`);
    try {
      // First get the product
      console.log("Making call to getProduct() function call to the contract ...");
      const productResult = await CONTRACT.methods.getProduct(epcCode).call();
      console.log("getProduct() function call response ...");
      console.log(productResult);

      // Not found scenario
      if(!productResult.epcCode) {
        callback(null, {error: new NotFound(`ecpCode: ${epcCode} not found!`), status: 404});
        return;
      }

      const product = {
        epcCode: productResult.epcCode,
        serialNumber: productResult.serialNumber,
        tagId: productResult.tagId,
        brand: productResult.brand,
        currentHolder: {
          id: productResult.currentHolder.id,
          name: productResult.currentHolder.name,
          participantType: participantTypes[productResult.currentHolder.participantType],
          location: productResult.currentHolder.location
        },
        shipments: []
      }

      console.log("Making call to getShipments() function call to the contract ...");
      const shipmentResult = await CONTRACT.methods.getShipments(epcCode).call();
      console.log("getShipments() function call response ...");
      console.log(shipmentResult);
      
      for(let i = 0 ; i < shipmentResult.length ; i++) {
        console.log('....', i);
        const result = shipmentResult[i];
        console.log(result);
        product.shipments.push(
          { 
            previousHolder: {
              id: result.previousHolder.id,
              name: result.previousHolder.name,
              participantType: participantTypes[result.previousHolder.participantType],
              location: result.previousHolder.location,
            }, 
            currentHolder: {
              id: result.currentHolder.id,
              name: result.currentHolder.name,
              participantType: participantTypes[result.currentHolder.participantType],
              location: result.currentHolder.location,
            } 
          }
        );
      }



      // await this.getProduct(epcCode, (product, error) => {
        console.log(product, "-------");
        // First check if the current holder is Retailer or not
        if(product.currentHolder.id != 4) {
          callback({validity: false}, null);
          return;
        }

        // Then check if there are total 3 shipments or not
        if(!product.shipments || product.shipments.length != 3) {
          callback({validity: false}, null);
          return;
        }

        const idealTransfer = [
          [1, 2],
          [2, 3],
          [3, 4]
        ];

        let validity = true;
        // Loop through the shipments to figure out of the product is genuine or not
        for(let i = 0 ; i < product.shipments.length; i++) {
          const shipment = product.shipments[i];
          if(shipment.previousHolder.id != idealTransfer[i][0] || shipment.currentHolder.id != idealTransfer[i][1]) {
            // callback({validity: false}, null);
            validity = false;
            break;
          }
        }

        callback({validity: validity}, null);
        
      // });

    } catch(err) {
      callback(null, err);
    }

  }
};
