const express = require('express');
const router = express.Router();
const truffle_connect = require('../connection/app.js');

// To pre-populate the block chain with dummy data
// Not to be called every time
router.get('/load-data', function(req, res, next) {
  truffle_connect.loadData((resp, err) => {
    if(resp) { // Success case
      res.send(resp);
    } 

    if(err) { // Failure case
      res.status(500);
      res.send(err);
    }
    
  });
});

// Add a new product to the block chain
router.post('/products', function(req, res, next) {
  truffle_connect.createProduct(req.body, (resp, err) => {
    if(resp) { // Success case
      res.send(resp);
    } 

    if(err) { // Failure case
      res.status(500);
      res.send(err);
    }
  });
});


// Get a product by epcCode from the block chain
router.get('/products/:epcCode', function(req, res, next) {
  truffle_connect.getProduct(req.params.epcCode, (resp, err) => {
    if(resp) { // Success case
      res.send(resp);
    } 

    if(err) { // Failure case
      if(err.status) {
        res.status(err.status);
      } else {
        res.status(500);
      }
      res.send(err.error);
    }
  });
});

// Add a shipment
router.post('/products/shipment', function(req, res, next) {
  truffle_connect.createShipment(req.body, (resp, err) => {
    if(resp) { // Success case
      res.send(resp);
    } 

    if(err) { // Failure case
      res.status(500);
      res.send(err);
    }
  });
});

// Verify a product
router.get('/verify-product/:epcCode', function(req,res, next) {
  truffle_connect.verifyProduct(req.params.epcCode, (resp, err) => {
    console.log(")))))))))) " + resp);
    if(resp) { // Success case
      res.send(resp);
    } 

    if(err) { // Failure case
      res.status(500);
      res.send(err);
    }
  });
});

module.exports = router;
