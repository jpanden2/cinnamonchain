## Project Setup
1. Install node js (LTS) from [here](https://nodejs.org/en/).
2. Install Ganache from [here](https://www.trufflesuite.com/ganache).
3. Clone the source code of this [repo](https://bitbucket.org/jpanden2/cinnamonchain/src/develop/).
    ```sh
    git clone https://pattnaik_soumitri@bitbucket.org/jpanden2/cinnamonchain.git
    ```
4. Change directory into the project source.
    ```sh
    cd cinnamonchain
    ```
5. Install project dependencies. (Make sure zscalar is turned off)
    ```sh
    npm install
    ```

## Running the application
1. Start Ganache (double click run).
2. (Optional) Make Ganache run on port `8545` if not already.
3. Change directory into the project source.
    ```sh
    cd cinnamonchain
    ```
4. Run.
    ```sh
    npm start
    ```

**Note:** Upon starting the application, the Ganache (local ethereum block chain) won't have any data. In-order to populate some dummy data, you can hit the **Load Data** API.


## API Overview

### Load Data API
This is a convenient which will load some dummy `Participants` & `Products` into Ganache.
**Request**
```sh
curl --location --request GET 'localhost:3000/api/load-data'
```
**Response**
```
Data loaded successfully!
```

**DUMMY DATA**

**Participants (4)**
```json
[
    {
        "id": 1,
        "name": "Levi Strauss & Co.",
        "participantType": "MAN",
        "location": "Banglore, India"
    },
    {
        "id": 2,
        "name": "UPS Supply Chain Solutions",
        "participantType": "LOG",
        "location": "Dubai, UAE"
    },
    {
        "id": 3,
        "name": "NRI DC",
        "participantType": "DC",
        "location": "Chicago, IL, USA"
    },
    {
        "id": 4,
        "name": "Levi's Store Georgetown",
        "participantType": "RET",
        "location": "Washinton, USA"
    }
]
```

**Products (2)**
```json
[
    {
        "epcCode": "epc1",
        "serialNumber": "sno1",
        "tagId": "tag1",
        "location": "Levi's",
        "currentHolder": {
            "id": "1",
            "name": "Levi Strauss & Co.",
            "participantType": "0",
            "location": "Banglore, India"
        },
        "shipments": []
    },
    {
        "epcCode": "epc2",
        "serialNumber": "sno2",
        "tagId": "tag2",
        "location": "Levi's",
        "currentHolder": {
            "id": "1",
            "name": "Levi Strauss & Co.",
            "participantType": "0",
            "location": "Banglore, India"
        },
        "shipments": []
    }
]
```

### Get product API
This API returns a product by `epcCode`.
**Request**
```sh
curl --location --request GET 'localhost:3000/api/products/epc1'
```
**Response**
```json
{
    "epcCode": "epc1",
    "serialNumber": "sno1",
    "tagId": "tag1",
    "brand": "Levi's",
    "currentHolder": {
        "id": "2",
        "name": "UPS Supply Chain Solutions",
        "participantType": "1",
        "location": "Dubai, UAE"
    },
    "shipments": [
        {
            "previousHolder": {
                "id": "1",
                "name": "Levi Strauss & Co.",
                "participantType": "0",
                "location": "Banglore, India"
            },
            "currentHolder": {
                "id": "2",
                "name": "UPS Supply Chain Solutions",
                "participantType": "1",
                "location": "Dubai, UAE"
            }
        }
    ]
}
```

### Create product API
This API creates product in Ganache.
**Request**
```sh
curl --location --request POST 'localhost:3000/api/products' \
--header 'Content-Type: application/json' \
--data-raw '{
    "epcCode": "epc5",
    "serialNumber": "sno5",
    "tagId": "tag5",
    "brand": "MRF",
    "currentHolderParticipantId": 1
}'
```

**Response**
```json
{
    "epcCode": "epc5",
    "serialNumber": "sno5",
    "tagId": "tag5",
    "brand": "MRF",
    "currentHolder": {
        "id": "1",
        "name": "Levi Strauss & Co.",
        "participantType": "0",
        "location": "Banglore, India"
    },
    "shipments": []
}
```

**Note** The `currentHolderParticipantId` parameter is optional. If not provided, if defaults to 1 i.e. for Manufacturers. The point of this parameter is to create a product with current holder as something other than manufacturers.


### Create shipment
This API creates a new shipment record for a product in Ganache.
**Request**
```sh
curl --location --request POST 'localhost:3000/api/products/shipment' \
--header 'Content-Type: application/json' \
--data-raw '{
    "epcCode": "epc1",
    "currentHolderParticipantId": "2"
}'
```

**Response**
```json
{
    "epcCode": "epc1",
    "serialNumber": "sno1",
    "tagId": "tag1",
    "brand": "Levi's",
    "currentHolder": {
        "id": "2",
        "name": "UPS Supply Chain Solutions",
        "participantType": "1",
        "location": "Dubai, UAE"
    },
    "shipments": [
        {
            "previousHolder": {
                "id": "1",
                "name": "Levi Strauss & Co.",
                "participantType": "0",
                "location": "Banglore, India"
            },
            "currentHolder": {
                "id": "2",
                "name": "UPS Supply Chain Solutions",
                "participantType": "1",
                "location": "Dubai, UAE"
            }
        },
        {
            "previousHolder": {
                "id": "2",
                "name": "UPS Supply Chain Solutions",
                "participantType": "1",
                "location": "Dubai, UAE"
            },
            "currentHolder": {
                "id": "2",
                "name": "UPS Supply Chain Solutions",
                "participantType": "1",
                "location": "Dubai, UAE"
            }
        }
    ]
}
```

### Verify Product
This API verifies if the product is genuine or not.
**Request**
```sh
curl --location --request GET 'localhost:3000/api/verify-product/epc1'
```

**Response**
```json
{
    "validity": true
}
```